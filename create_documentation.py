""" Small example that shows to load the data and create a measurement documentation. You must have a fully-featured Tex installation on your computer to use this feature!
"""
from pathlib import Path
from DMT.core import DutType, DocuDutLib, DutLib

path_to_folder = Path(__file__).resolve().parent
lib_save_dir = path_to_folder / "skywater130_lib"
lib = DutLib.load(lib_save_dir)

# To get a pdf with all information about the measurements DocuDutLib can be used:
# Be aware that this feature is currently in a early state and feedback/improvement suggestions are very welcome.
docu = DocuDutLib(lib, devices=[{"name": "esd_nfet_01v8"}])
docu.generate_docu(
    path_to_folder / "docu_skywater130_raw",
    plot_specs=[
        {"type": "id(vg)", "key": "IDVG", "dut_type": DutType.n_mos},
        {"type": "id(vg)", "key": "IDVG", "dut_type": DutType.p_mos},
        {"type": "id(vd)", "key": "IDVD", "dut_type": DutType.n_mos},
        {"type": "id(vd)", "key": "IDVD", "dut_type": DutType.p_mos},
    ],
    show=False,
    save_tikz_settings={
        "width": "3in",
        "height": "5in",
        "standalone": True,
        "svg": False,
        "build": True,
        "mark_repeat": 20,
        "clean": True,  # Remove all files except *.pdf files in plots
    },
)
