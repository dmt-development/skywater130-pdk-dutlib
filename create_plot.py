""" This script demonstrates how to load the measurement data and create a nice plot for some devices
"""
import numpy as np
from pathlib import Path
from DMT.core import DutLib, specifiers, Plot, DutType

path_to_folder = Path(__file__).resolve().parent
lib_save_dir = path_to_folder / "skywater130_lib"
lib = DutLib.load(lib_save_dir)

# These "specifiers" are used to access measurement data in a standardized way.
sp_vg = specifiers.VOLTAGE + ["G"]
sp_vb = specifiers.VOLTAGE + ["B"]
sp_vd = specifiers.VOLTAGE + ["D"]

sp_ig = specifiers.CURRENT + ["G"]
sp_ib = specifiers.CURRENT + ["B"]
sp_id = specifiers.CURRENT + ["D"]


# Now we plot all data of the "nfet_01v8" devices, one plot for every unique device length
device_flavor = "nfet_01v8"
plots = []
lengths = np.unique(
    [dut.length for dut in lib if dut.flavor == device_flavor]
)  # to compare all lengths
lengths = [0.5e-6]

for l_i in lengths:
    plot = Plot(
        f"length_{l_i*1e6:.2f}um_ID_w(VG)",
        x_specifier=sp_vg,
        y_label="$I_{\mathrm{D}}/w(mA/um)$",
        y_scale=1e3 / 1e6,
    )
    plot_mismatch = Plot(
        f"length_{l_i*1e6:.2f}um_ID_w(VG)_mismatch",
        x_specifier=sp_vg,
        y_label="$I_{\mathrm{D}}/w(mA/um)$",
        y_scale=1e3 / 1e6,
    )
    for dut in lib:
        if dut.dut_type == DutType.n_mos and np.isclose(dut.length, l_i):
            df = dut.data["T300.00K/IDVG"]
            df = df[np.isclose(df[sp_vb], 0)]
            df = df[np.isclose(df[sp_vd], 1.8)]

            if dut.die in [ #These dies contain "mismatched" devices: https://github.com/google/skywater-pdk-sky130-raw-data/issues/9#issuecomment-1204645147
                2602,
                2605,
                2607,
                2608,
                2611,
                2612,
                2618,
                2622,
                2624,
                2627,
            ]:
                plot_mismatch.add_data_set(
                    df[sp_vg],
                    df[sp_id] / dut.width,
                    label=f"w={dut.width*1e6:.2f}um, {dut.contact_info}",
                )
            else:
                plot.add_data_set(
                    df[sp_vg],
                    df[sp_id] / dut.width,
                    label=f"w={dut.width*1e6:.2f}um, {dut.contact_info}",
                )

    plots.append(plot)
    plots.append(plot_mismatch)

# open the plots interactively
for plt in plots[:-1]:
    plt.plot_pyqtgraph(show=False)

plots[-1].plot_pyqtgraph(show=True)

# save the plots instead: (uncomment plot command command above)
# plots[-1].save_tikz(
#     Path(__file__).parent.parent / "_static" / "readin_dut_lib",
#     standalone=True,
#     build=True,
#     clean=True,
#     width="6in",
#     legend_location="upper left",
# )
