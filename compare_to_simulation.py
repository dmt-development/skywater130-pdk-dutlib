""" Script that loads the measurement data and compares it to an Ngspice circuit simulation.
"""
from DMT.core.sweep import get_sweepdef
import numpy as np
from pathlib import Path
from DMT.core import DutLib, specifiers, Plot, McSkywater, DutType, Sweep, SimCon
from DMT.ngspice import DutNgspice

path_to_folder = Path(__file__).resolve().parent
lib_save_dir = path_to_folder / "skywater130_lib"
lib = DutLib.load(lib_save_dir)

# Define some "specifiers" for standardized measurement data access:
sp_vg = specifiers.VOLTAGE + ["G"]
sp_vb = specifiers.VOLTAGE + ["B"]
sp_vd = specifiers.VOLTAGE + ["D"]
sp_vdx = specifiers.VOLTAGE + ["DX"]

sp_ig = specifiers.CURRENT + ["G"]
sp_ib = specifiers.CURRENT + ["B"]
sp_id = specifiers.CURRENT + ["D"]


# Plot all data of the "nfet_01v8" devices, one plot for every unique device length
device_flavor = "nfet_01v8"
lengths = np.unique(
    [dut.length for dut in lib if dut.flavor == device_flavor]
)  # to compare all lengths
lengths = [0.5e-6]
widths = []
for length in lengths:
    widths.append(
        [
            dut.width
            for dut in lib
            if dut.flavor == device_flavor and np.isclose(dut.length, length)
        ]
    )

widths = np.unique([item for sublist in widths for item in sublist])

# Load the modelcard.
modelcard = McSkywater(
    pdk_path="/usr/local/share/pdk/sky130B/libs.tech/ngspice/sky130.lib.spice",
    pdk_corner="tt",
    default_module_name="sky130_fd_pr__" + device_flavor,
)

# Now we will create an array of "DutNgspice" objects that can be used to simulate a "Sweep" via DMT.core.
duts = []
for length in lengths:
    for width in widths:
        modelcard.set_values({"l": length * 1e6, "w": width * 1e6})
        duts.append(
            DutNgspice(
                None,
                DutType.n_mos,
                modelcard,
                nodes="D,G,GX,DX",
                reference_node="S",
                width=width,
                length=length,
            )
        )


# Now we will create a "Sweep" object that corresponds to the voltage sweep available in the measurement data.
# Then we can directly compare the circuit simulation to the measurement.
dut_flavor_example = next(
    dut
    for dut in lib
    if np.isclose(dut.width, width)
    and np.isclose(dut.length, length)
    and dut.dut_type == DutType.n_mos
)
measurement_data_example = dut_flavor_example.data["T300.00K/IDVG"]
sweep = Sweep.get_sweep_from_dataframe(
    measurement_data_example,
    inner_sweep_voltage=specifiers.VOLTAGE + ["G", "S"],
    outer_sweep_voltage=specifiers.VOLTAGE + ["D", "S"],
    temperature=300,
)

# We create a simulation controller and start all simulations:
sim_con = SimCon(t_max=300, n_core=1)
sim_con.append_simulation(dut=duts, sweep=sweep)
sim_con.run_and_read(force=True, remove_simulations=False)

# Now we plot all the generated data
plots = []
for length in lengths:
    plot = Plot(
        f"length_{length*1e6:.2f}um_ID_w(VG)",
        x_specifier=sp_vg,
        y_label="$I_{\mathrm{D}}/w(mA/um)$",
        y_scale=1e3 / 1e6,
        style="xtraction_color",
        legend_location="upper left",
    )
    plot_mismatch = Plot(
        f"length_{length*1e6:.2f}um_ID_w(VG)_mismatch",
        x_specifier=sp_vg,
        y_label="$I_{\mathrm{D}}/w(mA/um)$",
        y_scale=1e3 / 1e6,
        style="xtraction_color",
        legend_location="upper left",
    )
    for dut in lib:
        if dut.flavor == device_flavor and np.isclose(dut.length, length):
            df_meas = dut.data["T300.00K/IDVG"]
            df_meas = df_meas[np.isclose(df_meas[sp_vb], 0)]
            df_meas = df_meas[np.isclose(df_meas[sp_vd], 1.8)]

            dut_sim = next(
                dut_a
                for dut_a in duts
                if np.isclose(dut_a.width, dut.width)
                and np.isclose(dut_a.length, dut.length)
            )
            df_sim = dut_sim.get_data(sweep=sweep)
            df_sim = df_sim[np.isclose(df_sim[sp_vb], 0)]
            df_sim = df_sim[np.isclose(df_sim[sp_vdx], 1.8)]

            if dut.die in [
                2602,
                2605,
                2607,
                2608,
                2611,
                2612,
                2618,
                2622,
                2624,
                2627,
            ]:
                plot_mismatch.add_data_set(
                    df_meas[sp_vg],
                    df_meas[sp_id] / dut.width,
                    label=f"w={dut.width*1e6:.2f}um, {dut.contact_info}",
                )
                plot_mismatch.add_data_set(
                    df_sim[sp_vg],
                    df_sim[sp_id] / dut.width,
                    # label=f"sim w={dut.width*1e6:.2f}um, {dut.contact_info}",
                )
            else:
                plot.add_data_set(
                    df_meas[sp_vg],
                    df_meas[sp_id] / dut.width,
                    label=f"w={dut.width*1e6:.2f}um, {dut.contact_info}",
                )
                plot.add_data_set(
                    df_sim[sp_vg],
                    df_sim[sp_id] / dut.width,
                    # label=f"sim w={dut.width*1e6:.2f}um, {dut.contact_info}",
                )

    plots.append(plot)
    plots.append(plot_mismatch)

for plt in plots[:-1]:
    plt.plot_pyqtgraph(show=False)

plots[0].plot_pyqtgraph(show=True)

# plots[0].save_tikz(
#     Path(__file__).parent / "_static" / "compare_to_simulation",
#     standalone=True,
#     build=True,
#     svg=True,
#     clean=True,
#     width="6in",
#     legend_location="upper left",
# )
