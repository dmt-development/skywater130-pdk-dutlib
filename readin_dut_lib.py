""" Script that reads the skywater130 data into a DMT.DutLib. The DutLib object can conveniently used in Python and is based on the Pandas library. 

This example demonstrates how to read in the skywater130 pdk measurement data into a DMT DutLib object. The data processing can then be done in Python. 
Feel free to use this example as a starting point for bulk reading of data.

Before you can use this script, you have to clone the measurement data repository from:
https://github.com/google/skywater-pdk-sky130-raw-data
and adjust the paths at the top of the script.

The file structure of the Skywater PDK does not directly fit the file strucutre assumed by DMT bulk-read routines. 
DMT assumes that 1 folder holds measurement data of exactly one physical device. 
As this is not the case for the skywater data, we must first rearrange the measurement data accordingly. 
At the end of this script, the DutLib object is saved in hdf5 format under skywater130_lib/.

"""
# Copyright 2022 SemiMod

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import re
from pathlib import Path
from DMT.core import DutMeas, DutType, DutLib

path_to_folder = Path(__file__).resolve().parent
path_to_git = (
    path_to_folder / "skywater-pdk-sky130-raw-data"
)  # adjust this path locally so that the Skywater repo is found
path_to_cells = path_to_git / "sky130_fd_pr" / "cells"

# sort the measurement data files into folders containing exactly one device using shutil
for child in path_to_cells.glob("*/" * (2)):
    if (
        child.is_file()
    ):  # only files are allowed since directories are created this does not interfere
        device_name = child.stem[: child.stem.find("_ID")]
        target_folder = child.parent / device_name
        target_folder.mkdir(exist_ok=True)
        shutil.move(child, target_folder / child.name)


# Create a DutLib object that is used to store the measurement data
lib_save_dir = path_to_folder / "skywater130_lib"
lib = DutLib(save_dir=lib_save_dir, force=True)
lib.wafer = "MPW-1"

# for importing all data at once, DutLib can use the import_directory method. This method requires a custom filter function to create the correct DutViews, each holding data of one device and corresponding miscellaneous data such as contact configurations etc.
def filter_dut(dut_name):
    """Create DutView objects from the different folder names. Infer information from folder structure of Skywater.

    Parameters
    ----------
    dut_name : str
        Path to the dut folder.

    Returns
    -------
    DMT.core.DutView
        DutView which should represent the device represented by a folder of measurements.
    """
    dut_path = Path(dut_name)
    flavor = dut_path.parent.name
    width = re.search(r"_w([\dp]+)u_", dut_path.name, re.MULTILINE).group(1)
    width = float(width.replace("p", ".")) * 1e-6
    length = re.search(r"_l([\dp]+)u_", dut_path.name, re.MULTILINE).group(1)
    length = float(length.replace("p", ".")) * 1e-6
    dut_type = DutType.n_mos if "nfet" in flavor else DutType.p_mos
    multiplication_factor = int(
        re.search(r"_m([\d]+)\(", dut_path.name, re.MULTILINE).group(1)
    )
    module_name = int(re.search(r"\((\d+)_", dut_path.name, re.MULTILINE).group(1))
    contacts = re.search(r"_m[\d]+\((.+)", dut_path.name, re.MULTILINE).group(1)

    dut = DutMeas(
        database_dir=path_to_folder / "tmp",
        dut_type=dut_type,
        force=True,
        wafer="MPW-5",
        die=module_name,
        width=width,
        length=length,
        contact_config="SGD",
        name=dut_path.name,
        reference_node="S",
        ndevices=multiplication_factor,
        flavor=flavor,  # not yet supported in current DMT-main branch
    )
    dut.contact_info = contacts.replace("_", " ")

    return dut


# In DMT, each measurement is assigned a key that can be used to access it. The key MUST hold the ambient measurement temperature.
# This function is passed to "import_directory" for adding temperature information to the data, since this is handled different by all fabs.
def key_generator(key_part):
    """This method adds temperature information to measurement data keys.

    The argument of this function usually has the following structure:
    DEVICE_FOLDER/XXTEMPXX/measurement_name

    Parameters
    ----------
    key_part : str
        The part of the key which may contain the temperature

    Returns
    -------
    float or str
        Should return -1 if no temperature is obtained and otherwise return the temperature in Kelvin.
        If a string is returned instead, it is directly used as the current key of a given measurement data set.

    Raises
    ------
    NotImplementedError
        In case, new measurements have different structures.
    """
    if "IDVG" in key_part:
        return "T300.00K/IDVG"
    elif "IDVD" in key_part:
        return "T300.00K/IDVD"
    else:
        raise NotImplementedError


# Import the measurements using the dut filter function in parallel. See the method`s documentation in DMT.core.
lib.n_jobs = 6  # number of parallel jobs
lib.import_directory(
    import_dir=path_to_cells,
    dut_filter=filter_dut,
    dut_level=2,
    force=True,
    temperature_converter=key_generator,
)

# "Clean" the data so that DMT standardized names for electrical quantities are used.
for dut_a in lib:
    dut_a.clean_data()

lib.dut_ref = lib.duts[
    0
]  # does not matter for this script but is needed for data saving and documentation.

# The data is now read for further use
# to save just use:
lib.save()
# to load in a different script:
# lib = DutLib.load(lib_save_dir)
