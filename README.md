# Skywater130-pdk-dutlib


## Description

This repository demonstrates how to read and analyze the [skywater-pdk-sky130-raw-data](https://github.com/google/skywater-pdk-sky130-raw-data) using the Python project DMT-core. For this purpose, four exemplary scripts are provided.
DMT-core is [documented here](https://gitlab.com/dmt-development/dmt-core) and provides semiconductor device modeling and characterization relevant routines in Python. 

## Usage 

First, install at least version 1.7.0 of [DMT](https://gitlab.com/dmt-development/dmt-core) from PyPi.  Then, get a copy of the Skywater data. There are two options:

- Download the latest `DMT.DutLib` from the [releases](https://gitlab.com/dmt-development/skywater130-pdk-dutlib/-/releases) of this repo.
- Clone the official [Skywater130](https://github.com/google/skywater-pdk-sky130-raw-data) repo and adjust the read-in script "read_dut_lib.py" of this repo to your needs. Currently this project releases the DutLib for the raw data committed in `4545e19cc0f1cb54ef4fdc142669567e33a2cea8`. 

The first option is probably the most convenient for most users. The file should then be put into the folder "skywater130_lib". For expert users, if you apply the script to newer commits, we are happy if you offer a pull requests.

## Script 1: readin_dut_lib.py

This script reads all measurement data of the technology into a "DutLib" object that can be saved and is ideal for usage in Python. 
For this script to work, you must download the measurement data first and adjust the path at the top of the script accordingly, so that it is found.

## Script 2: create_plot.py

This script demonstrates how to load the measurement data in Python and create a nice plot for some devices. If this works for you, measurement analysis in Python 
becomes easy.

## Script 3: create_documentation.py

This script creates a TeX documentation of all the measurement data, it is perfect to get an overview. Note that a fully featured TeX installation is required for this script to work. 

## Script 4: compare_to_simulation.py

The script [compare_to_simulation.py](compare_to_simulation.py) reads some measurement data and runs corresponding circuit simulations for comparison using Ngspice. 
As a pre-requisite:

1) make sure you have a recent version of ngspice installed that is also located in your PATH variable
2) install the [open_pdks](http://opencircuitdesign.com/open_pdks/) skywater130 PDK to your system
3) try to run the ngspice circuit "ngspice_circuit_example.ckt" using the command "ngspice -b ngspice_circuit.ckt" in a Terminal

If points 1-3 are fulfilled, you can run the Python script, the result should be shown below:

![Comparing simulation to measurement of nfet_01v8 devices with length=0.5um](_static/compare_to_simulation/length_0_dot_50um_ID_wVG.svg "Measurement vs. Model plot")



## Support
If you have any questions or issues regarding DMT and its usage raise an issue [here](https://gitlab.com/dmt-development/skywater130-pdk-dutlib/-/issues) or [at the main DMT repository](https://gitlab.com/dmt-development/dmt-core/-/issues/new?issuable_template=question).
If you have any questions or issues regarding the measurement data, head over to the [respective Skywater repository](https://github.com/google/skywater-pdk-sky130-raw-data).

## Contributing
If you can propose other data sorting algorithms or more extensive documentation w.r.t. the measurement data, pull requests and issues are always welcome.

## License
The resources in this repository are released under the Apache 2.0 license.

The copyright details (which should also be found at the top of every file) are;

```
Copyright 2022 SemiMod

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

